import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Corona Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class MonitoringPoint {
  String deviceName;
  String deviceId;
  int rssi;
  DateTime timestamp;

  MonitoringPoint(ScanResult scanResult) {
    this.deviceId = scanResult.device.id.toString();
    this.deviceName = scanResult.device.name;
    this.rssi = scanResult.rssi;
    this.timestamp = new DateTime.now();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  FlutterBlue flutterBlue;
  List<MonitoringPoint> devices = new List();

  _MyHomePageState() {
    flutterBlue = FlutterBlue.instance;

    // Listen to scan results
    flutterBlue.scanResults.listen((scanResult) {
      // do something with scan result
      scanResult.forEach((result) {
        devices.removeWhere((monitoringPoint) =>
            monitoringPoint.deviceId == result.device.id.toString());
        devices.add(new MonitoringPoint(result));
      });
      devices.sort((monitoringPoint1, monitoringPoint2) =>
          monitoringPoint2.rssi.compareTo(monitoringPoint1.rssi));
    });

    const rescanTime = const Duration(milliseconds: 900);
    new Timer.periodic(rescanTime, (Timer t) => this._rescanDevices());
  }

  void _rescanDevices() {
    flutterBlue.startScan(timeout: Duration(milliseconds: 900));
    flutterBlue.stopScan();
    this.setState(() {});
  }

  List<Widget> getDevicesTable() {
    List<Widget> table =  devices
        .map((item) => new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text('${item.deviceId.substring(0, 6)}'),
                  new Text(
                      '${item.deviceName.length > 8 ? item.deviceName.substring(0, 8) : item.deviceName}'),
                  new Text('${item.rssi}'),
                  new Text(
                      '${new DateTime.now().difference(item.timestamp).inSeconds}')
                ]))
        .toList();
    table.insert(0, new Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        new Text(
          'ID',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        new Text(
          'NAME',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        new Text(
          'RSSI',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        new Text(
          'Sago',
          style: TextStyle(fontWeight: FontWeight.bold),
        )
      ],
    ));
    return table;
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(15.0),
                child: new Column(children:
                  getDevicesTable()
                ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _rescanDevices,
        tooltip: 'Rescan Devices',
        child: Icon(Icons.refresh),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
